﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

    public GameObject obj;

	// Use this for initialization
	void Start () {
		
	}


    void handleInput(Vector3 position)
    {
        /*Destroy(*/
        Instantiate(obj, Camera.main.ScreenToWorldPoint(position) + 10 * Vector3.forward, Quaternion.identity);//, 2);
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButtonUp(0))
        {
            handleInput(Input.mousePosition);
        }
    }
}
