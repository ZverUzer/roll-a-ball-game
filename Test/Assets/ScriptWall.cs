﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptWall : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.AddComponent<Rigidbody>();
                cube.transform.position = new Vector3(i, j, 0);
            }
        }
    }
}
