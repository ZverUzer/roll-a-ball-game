﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global {
	public static int levelNumber = 1;

	public static int getLevelScore(){
		if (levelNumber == 1) {
			return 10;
		}
		if (levelNumber == 2) {
			return 12;
		}
		if (levelNumber == 3) {
			return 14;
		}

		return 2;
	}
}
