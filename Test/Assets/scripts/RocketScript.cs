﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketScript : MonoBehaviour
{
    public Rigidbody rocket;
    public float speed = 10f;
    public Rigidbody copy;

    void Start()
    {
        for (int i = 0; i < 20; i++)
        {
            float angle = i * Mathf.PI * 2 / 20;
            Vector3 pos = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * 2f;
            Instantiate(copy, pos, Quaternion.identity);
        }
    }

    void FireRocket()
    {
        Rigidbody rocketClone = (Rigidbody)Instantiate(rocket, transform.position, transform.rotation);
        rocketClone.velocity = transform.forward * speed;

        // You can also acccess other components / scripts of the clone
        //rocketClone.GetComponent<MyRocketScript>().DoSomething();
    }

    void handleInput(Vector3 position)
    {
        Destroy(Instantiate(rocket, Camera.main.ScreenToWorldPoint(position) + 10 * Vector3.forward, Quaternion.identity), 2);
    }

    // Calls the fire method when holding down ctrl or mouse
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            FireRocket();
        }

        //if (Input.GetMouseButtonUp(0))
        //{
        //    handleInput(Input.mousePosition);
        //}
    }
}
