﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

public class CameraScript : MonoBehaviour
{
    public Transform player;
    public float turnSpeed = 1.5f;

    private Vector3 offset;

    // Use this for initialization
    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    private void LateUpdate()
    {
		offset = Quaternion.AngleAxis(CnInputManager.GetAxis("HorizontalCam") * turnSpeed, Vector3.up) * offset;
        transform.position = player.position + offset;
        transform.LookAt(player.position);
    }
}
