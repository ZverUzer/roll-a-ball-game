﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using CnControls;
using UnityEngine.SceneManagement;

public class BallScript : MonoBehaviour
{
    public Renderer renderer;
	public GameObject disapearPlatform;
	public GameObject appearPlatform;
    private Rigidbody rb;
	public GameObject portal;
    private int counter;
    public Text countText;
    public Text winText;
    public Transform camera;
	public float speed = 1.5f;

    // As an example, we turn the game object into a wreck after 3 seconds automatically
    void Start()
	{
        rb = GetComponent<Rigidbody>();
        counter = 0;
        UpdateScoreText();
        winText.text = "";
		portal.SetActive (false);

		if (appearPlatform != null) {
			appearPlatform.SetActive (false);
		}
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontalMovement = CnInputManager.GetAxis("Horizontal");
        float verticalMovement = CnInputManager.GetAxis("Vertical");

		Vector3 movementVector = new Vector3(horizontalMovement*speed, 0.0f, verticalMovement*speed);
        Vector3 relativeVector = camera.TransformVector(movementVector);

        rb.AddForce(relativeVector);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            counter++;
            UpdateScoreText();
        }
		if (other.gameObject.CompareTag ("Portal")) {
			Global.levelNumber++;
			if (Global.levelNumber > 4) {
				Global.levelNumber = Random.Range (1, 3);
				SceneManager.LoadScene (Global.levelNumber);

				return;
			}
			SceneManager.LoadScene (Global.levelNumber);
		}
		if (other.gameObject.CompareTag ("Loose")) {
			winText.text = "Вы проиграли!";
			SceneManager.LoadScene (Global.levelNumber);
		}
    }

    private void UpdateScoreText()
    {
        countText.text = "Счёт: " + counter.ToString();

		if (counter == Global.getLevelScore())
        {
            winText.text = "Открыт портал на следующий уровень!";
			portal.SetActive (true);
			if (disapearPlatform != null) {
				disapearPlatform.SetActive (false);
			}
			if (appearPlatform != null) {
				appearPlatform.SetActive (true);
			}
        }
    }
}
